<?php

class Blog_Model_DbTable_BlogComments extends Zend_Db_Table_Abstract {
    
    protected $_name = 'BlogComments';
    protected $_primary = 'id';
   
    public function getAll() {
        $select = $this->select()
                ->from($this->_name, array('id', 'username', 'email', 'status', 'date', 'comment', 'username', 'email'))
                ->order('date DESC');
        return $this->getAdapter()->fetchAll($select);
    }
    
    public function getLastComments() {
        $select = $this->select()
                       ->setIntegrityCheck(false)
                       ->from($this->_name, array('id', 'username', 'email', 'status', 'date', 'diyId'))
                       ->join('Diy', 'Diy.id = diyId', array('alias'))
                       ->limit(20)
                       ->order('date DESC');
        return $this->getAdapter()->fetchAll($select);
    }
    
    public function countNewComments() {
        $select = $this->select()
                ->from($this->_name, array('COUNT(*)'))
                ->where('new = 1');
        return $this->getAdapter()->fetchOne($select);
    }
    
    public function getCommentsByDiyId($diyId) {
        $select = $this->select()
                ->from($this->_name, array('id', 'username', 'comment', 'reply', 'date'))
                ->where('status = 1')
                ->where('diyId = ?', $diyId)
                ->order('date');
        return $this->getAdapter()->fetchAll($select);
    }
    
    public function getByDiyId($diyId) {
        $select = $this->select()
                ->from($this->_name, array('id', 'username', 'comment', 'reply', 'status', 'date', 'email'))
                ->where('diyId = ?', $diyId)
                ->order('date');
        return $this->getAdapter()->fetchAll($select);
    }
    
    public function getPairs() {
        $select = $this->select()
                ->from($this->_name, array('id', 'diyId'));
        return $this->getAdapter()->fetchPairs($select);
        
    }
}
