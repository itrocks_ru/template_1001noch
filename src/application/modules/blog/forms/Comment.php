<?php

class Blog_Form_Comment extends ItRocks_Form {

    public function init() {
        $this->setDecorators(array('FormElements', 'Form'));
        $view = Zend_Layout::getMvcInstance()->getView();


        $textDecorator = new ItRocks_Form_Decorator_Text;
        $captchaDecorator = new ItRocks_Form_Decorator_Captcha;
        $textAreaDecorator = new ItRocks_Form_Decorator_TextArea;

        $this->addElement($this->createElement('text', 'username', array(
            'required' => true,
            'label' => 'blogUsername',
            'class' => 'form-control',
            'decorators' => array($textDecorator)
        )));

        $this->addElement($this->createElement('text', 'email', array(
            'required' => true,
            'label' => 'blogEmail',
            'class' => 'form-control',
            'validators' => array(
                'EmailAddress'
            ),
            'decorators' => array($textDecorator)
        )));

        $this->addElement($this->createElement('text', 'comment', array(
            'required' => true,
            'label' => 'blogComment',
            'class' => 'form-control',
            'decorators' => array($textAreaDecorator)
        )));


        $this->addElement($this->createElement('submit', 'submit', array(
            'label' => 'blogAddComment',
            'class' => 'btn btn-primary'
        )));

    }


}
