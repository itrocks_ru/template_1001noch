<?php

class Blog_AdminController extends Zend_Controller_Action {

    protected $_diy = null;
    protected $_tags = '';

    public function init() {
        $this->_helper->layout->setLayout('admin');
        $action = $this->_request->getActionName();

        if (in_array($action, array('edit', 'delete', 'view-comments', 'delete-comment', 'show-comment'))) {
            $diyTable = new Blog_Model_DbTable_Blog;

            $alias = $this->_request->getParam('alias');
            $this->_diy = $diyTable->getByAlias($alias);
            if (!$this->_diy){
                $this->_diyNotFound();
            }
        }
    }
    
    public function indexAction() {
        $page = $this->_request->getParam('page', 1);
        
        $diyModel = new Blog_Model_Blog;
        $items = $diyModel->getAllWithComments();
        
        $paginator = Zend_Paginator::factory($items);
        $paginator->setItemCountPerPage(20);
        $paginator->setCurrentPageNumber($page);

        $this->view->assign('paginator', $paginator);
    }

    public function addAction()  {
        $diyTable = new Blog_Model_DbTable_Blog;
        $diy = $diyTable->createRow();
        $blogTags = new Blog_Model_Tags();
        $form = new Blog_Form_Blog($diy, $this->existingTags($diy), $this->existingDate($diy));
        $tagsTable = new Model_DbTable_Tags();
        $transliterateModel = new Model_Transliterate();

        if ($this->_request->isPost()) {
            $formData = $this->_request->getPost();
            if ($form->isValid($formData)) {
                try {
                    if (!empty($_FILES['image']['name'])) {
                        $fileUpload = new Model_FileUpload();
                        $formData['image'] = $fileUpload->uploadDiyPhoto($_FILES['image']['tmp_name'], $_FILES['image']['name']);
                    } else {
                        $formData['image'] = '';
                    }
                    if (empty($formData['alias'])) {
                        $formData['alias'] = $transliterateModel->transliterate($formData['title']);
                    }
                    if (empty($formData['metaTitle'])) {
                        $formData['metaTitle'] = $transliterateModel->transliterate($formData['title']);
                    }
                    $formData['date'] = date('Y-m-d', strtotime($formData['date']));
                    $formData['date'] = date('Y-m-d H:i:s');

                    $diy->setFromArray($formData);
                    $id = $diy->save();

                    $blogTags->updateTagsByArticle($id, $formData['tags']);

                    $this->_helper->flashMessenger->setNamespace('messages')
                        ->addMessage($this->view->translate('added'));

                    $this->_helper->redirector('index', 'admin', 'blog');
                } catch (Exception $e) {
                    $this->view->assign('error', $e->getMessage());
                    $this->_helper->redirector('add', 'admin', 'blog');
                }
            }
        }
        $this->view->assign('tags', $tagsTable->getAllTags());
        $this->view->assign('form', $form);
    }

    public function editAction() {
        $transliterateModel = new Model_Transliterate();
        $form = new Blog_Form_Blog($this->_diy, $this->existingTags($this->_diy), $this->existingDate($this->_diy));
        $blogTags = new Blog_Model_Tags();

        if ($this->_request->isPost()) {
            $formData = $this->_request->getPost();
            if ($form->isValid($formData)) {
                try {
                    if (!empty($_FILES['image']['name'])) {
                        $fileUpload = new Model_FileUpload();
                        $formData['image'] = $fileUpload->uploadDiyPhoto($_FILES['image']['tmp_name'], $_FILES['image']['name']);
                    } else {
                        $formData['image'] = $this->_diy->image;
                    }
                    if (empty($formData['alias'])) {
                        $formData['alias'] = $transliterateModel->transliterate($formData['title']);
                    }
                    if (empty($formData['metaTitle'])) {
                        $formData['metaTitle'] = $transliterateModel->transliterate($formData['title']);
                    }
                    $formData['date'] = date('Y-m-d', strtotime($formData['date']));
                    $dateDb = explode(' ', $this->_diy->date);
                    if ($formData['date'] != $dateDb[0]){
                        $formData['date'] .= date(' H:i:s');
                    } else{
                        $formData['date'] .= ' '.$dateDb[1];
                    }
                    $this->_diy->setFromArray($formData);
                    $id = $this->_diy->save();
                    $blogTags->updateTagsByArticle($id, $formData['tags']);

                    $this->_helper->flashMessenger->setNamespace('messages')
                        ->addMessage($this->view->translate('edited'));

                    $this->_helper->redirector('index', 'admin', 'blog');
                } catch (Exception $e) {
                    $this->view->assign('error', $e->getMessage());
                }
            }
        }

        $this->view->assign('form', $form);
    }
    
    public function deleteAction() {       
        $this->_diy->delete();
        $this->_helper->redirector('index', 'admin', 'blog');
    }
    
    public function viewCommentsAction() {
        $alias = $this->_request->getParam('alias');

        $diyCommentsTable = new Blog_Model_DbTable_BlogComments();
        
        $this->view->assign('comments', $diyCommentsTable->getByDiyId($this->_diy->id));
        $this->view->assign('diy', $this->_diy);
        $this->view->assign('alias', $alias);
    }
    
    public function showCommentAction() {
        $diyCommentsTable = new Blog_Model_DbTable_BlogComments();
        $comment = $diyCommentsTable->find($this->_request->getParam('commentId'))->current();
        
        if (!$comment)
            $this->_commentNotFound(); 
       
        $form = new Blog_Form_AdminComment($comment);
        
        if ($this->_request->getPost()) {
            $formData = $this->_request->getPost();
            if ($form->isValid($formData)) {
                $formData['new'] = 0;
                $comment->setFromArray($formData);
                $comment->save();
                $this->_helper->redirector('view-comments', 'admin', 'blog', array('alias' => $this->_diy->alias));
            }
        }
        
        $this->view->assign('form', $form);
    }
    private function existingTags($diy){
        $tags = '';
        if($diy->id > 0)
        {
            $tagsModel          = new Blog_Model_Tags();
            $tags = $tagsModel->getAllTagsFromArticle($diy->id, true);
        }
        return $tags;
    }
    private function existingDate($diy){
        $dateDb = explode(' ', $diy->date);
        $postDate = date('m/d/Y');
        if ($diy->date) {
            $postDate = date('m/d/Y', strtotime($dateDb[0]));
        }
        return $postDate;
    }
    
    public function deleteCommentAction() {
        $diyCommentsTable = new Blog_Model_DbTable_BlogComments();
        $comment = $diyCommentsTable->find($this->_request->getParam('commentId'))->current();
        
        if (!$comment)
            $this->_commentNotFound(); 
            
        $comment->delete();
        $this->_helper->redirector('view-comments', 'admin', 'blog', array('alias' => $this->_diy->alias));
    }
    
    protected function _diyNotFound() {
        $this->_helper->flashMessenger->setNamespace('errorMessages')
                                      ->addMessage($this->view->translate('diyNotFound'));
        $this->_helper->redirector('index', 'admin', 'blog');
    }
    
    protected function _commentNotFound() {
        $this->_helper->flashMessenger->setNamespace('errorMessages')
                                      ->addMessage($this->view->translate('commentNotFound'));
        $this->_helper->redirector('view-comments', 'admin', 'blog', array('id' => $this->_diy->id));
    }
}
