<?php
class Services_Bootstrap extends Zend_Application_Module_Bootstrap {
    protected function _initRouter(){
        $frontController = Zend_Controller_Front::getInstance();
        $router = $frontController->getRouter();


        $router->addRoute('/services', new Zend_Controller_Router_Route(
            '/services/:alias', array(
            'module' => 'services',
            'controller' => 'index',
            'action' => 'index',
            'alias' => null
        )));

        $router->addRoute('/services/showId', new Zend_Controller_Router_Route(
            '/services/show/id/:id', array(
            'module' => 'services',
            'controller' => 'index',
            'action' => 'show'
        )));

        $router->addRoute('/services/showAlias', new Zend_Controller_Router_Route(
            '/services/show/:alias', array(
            'module' => 'services',
            'controller' => 'index',
            'action' => 'show',
            'alias' => null
        )));

        $router->addRoute('/services/admin', new Zend_Controller_Router_Route(
            '/services/admin', array(
                'module' => 'services',
                'controller' => 'admin',
                'action' => 'index'
            )
        ));

        /* Static routes from old site */
        $router->addRoute('/services/fizicheskim_licam', new Zend_Controller_Router_Route(
            '/fl.html', array(
                'module' => 'services',
                'controller' => 'index',
                'action' => 'index',
                'alias' => 'uslugi-dlya-grazhdan'
            )
        ));
        $router->addRoute('/services/individualnim-predprinimatelyam', new Zend_Controller_Router_Route(
            '/ip.html', array(
                'module' => 'services',
                'controller' => 'index',
                'action' => 'index',
                'alias' => 'individualnim-predprinimatelyam'
            )
        ));
        $router->addRoute('/services/uridicheskim_licam', new Zend_Controller_Router_Route(
            '/ul.html', array(
                'module' => 'services',
                'controller' => 'index',
                'action' => 'index',
                'alias' => 'uridicheskim_licam'
            )
        ));
        $router->addRoute('/services/registraciya_ip_ul', new Zend_Controller_Router_Route(
            '/registraciya.html', array(
                'module' => 'services',
                'controller' => 'index',
                'action' => 'index',
                'alias' => 'registraciya_ip_ul'
            )
        ));
        $router->addRoute('/services/abonentskoe_obsluzhivanie_ip_ul', new Zend_Controller_Router_Route(
            '/ao.html', array(
                'module' => 'services',
                'controller' => 'index',
                'action' => 'index',
                'alias' => 'abonentskoe_obsluzhivanie_ip_ul'
            )
        ));
        $router->addRoute('/services/razrabotka_biznes_planov', new Zend_Controller_Router_Route(
            '/b-p.html', array(
                'module' => 'services',
                'controller' => 'index',
                'action' => 'index',
                'alias' => 'razrabotka_biznes_planov'
            )
        ));
        return $router;
    }
}