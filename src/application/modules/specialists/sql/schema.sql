DROP TABLE IF EXISTS `Specialists`;
CREATE TABLE `Specialists` (
  `id` int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `fio` VARCHAR(250) NOT NULL,
  `photo1` VARCHAR(250) NOT NULL,
  `photo2` VARCHAR(250) NOT NULL,
  `photoSmall` VARCHAR(250) NOT NULL,
  `shortDescription` TEXT NOT NULL,
  `description` TEXT NOT NULL,
  `position` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
