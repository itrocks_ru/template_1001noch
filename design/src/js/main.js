//= partials/full-width-slider.js
//= partials/full-width-horizontal-slider.js
//= partials/gallery-slider.js
//= partials/reviews-slider.js
//= partials/room-numbers.js

$('#slider-main-form').submit(function() {
    var phone = $('#slider-form-phone').val();
    var fio = $('#slider-form-fio').val();

    $.ajax({
        url: config.hotelroom.request,
        method: 'POST',
        data: {
            phone: phone,
            fio: fio
        },
        dataType: 'json',
        success: function(response) {
            if (typeof response.success !== 'undefined') {
                location.href = config.hotelroom.successUrl;
            } else if (typeof response.error !== 'undefined') {
                if (typeof response.error.fio !== 'undefined') {
                    $('#slider-fio-error').html(response.error.fio);
                }
                if (typeof response.error.phone !== 'undefined') {
                    $('#slider-phone-error').html(response.error.phone);
                }
            }
        },
        error: function(errors) {
            $('#booking-common-error').html('Произошла непридвиненная ошибка. Приносим извинения');
            return false;
        }
    });
    return false;
});
$('.index-contacts-content-btn').click(function () {
    $('#bookingModal').modal('toggle');
    return false;
});