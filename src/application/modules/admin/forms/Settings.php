<?php

class Admin_Form_Settings extends ItRocks_Form  {

    protected $_mainSettings = array();
    protected $_social = array();
    protected $_contacts = array();
    protected $_discountSettings = array();

    public function __construct(array $mainSettings, $social, $contacts, $discountSettings) {
        $this->_mainSettings    = $mainSettings;
        $this->_social          = $social;
        $this->_contacts        = $contacts;
        $this->_discountSettings = $discountSettings;
        parent::__construct();
    }

    public function init() {
        $this->setDecorators(array('FormElements', 'Form'));
        $this->setAttrib('class', 'form-horizontal');
        $this->setAttrib('enctype', 'multipart/form-data');

        $textDecorator      = new ItRocks_Form_Decorator_AdminText;
        $textareadDecorator = new ItRocks_Form_Decorator_AdminTextarea;
        $buttonDecorator    = new ItRocks_Form_Decorator_AdminSubmit;
        $urlValidator       = new ItRocks_Validate_ExternalUrl();
        $mailValidator      = new Validator_Mail();
        $phoneValidator     = new ItRocks_Validate_Phone();
        $adminFileDecorator = new ItRocks_Form_Decorator_AdminFile();

        $this->addElement($this->createElement('text', 'title', array(
            'required' => true,
            'label' => 'setTitle',
            'value' => $this->_mainSettings['title'],
            'readonly' => false,
            'class' => 'span8',
            'placeholder' => 'title',
            'decorators' => array($textDecorator)
        )));

//        $this->addElement($this->createElement('file', 'logo', array(
//            'required' => ($this->_mainSettings['siteLogo']) ? false : true,
//            'label' => 'siteLogo',
//            'decorators' => array($adminFileDecorator),
//            'validators' => array(
//                array('Extension', false, 'jpg,jpeg,png,gif')
//            )
//        )));

//        $imageHelper = new Model_Images_Common();
//        $this->logo->addFilter($imageHelper);
//        if ($this->_mainSettings['siteLogo']) {
//            $this->logo->addDecorator(new ItRocks_Form_Decorator_ImageView(array(
//                'imageUrl' => $imageHelper->url($this->_mainSettings['siteLogo']),
//                'imageAlternate' => ''
//            )));
//        }

//        $this->addElement($this->createElement('file', 'favIcon', array(
//            'required' => ($this->_mainSettings['siteFavicon']) ? false : true,
//            'label' => 'favIcon',
//            'decorators' => array($adminFileDecorator),
//            'validators' => array(
//                array('Extension', false, 'png, gif')
//            )
//        )));
//
//        $this->favIcon->addFilter($imageHelper);
//        if ($this->_mainSettings['siteFavicon']) {
//            $this->favIcon->addDecorator(new ItRocks_Form_Decorator_ImageView(array(
//                'imageUrl' => $imageHelper->url($this->_mainSettings['siteFavicon']),
//                'imageAlternate' => ''
//            )));
//        }

        $this->addElement($this->createElement('text', 'keywords', array(
            'required' => true,
            'label' => 'setKeywords',
            'value' => $this->_mainSettings['keywords'],
            'readonly' => false,
            'class' => 'span8',
            'placeholder' => 'keywords',
            'decorators' => array($textDecorator)
        )));

        $this->addElement($this->createElement('text', 'description', array(
            'required' => true,
            'label' => 'setDescription',
            'value' => $this->_mainSettings['description'],
            'class' => 'span8',
            'data-size' => '160',
            'decorators' => array($textareadDecorator)
        )));

        $this->addElement($this->createElement('text', 'vk', array(
            'required' => false,
            'label' => 'socialVk',
            'value' => $this->_social['vk'],
            'readonly' => false,
            'class' => 'span8',
            'placeholder' => 'Пример: https://vk.com/spleanru',
            'validators' => array($urlValidator),
            'decorators' => array($textDecorator)
        )));

        $this->addElement($this->createElement('text', 'facebook', array(
            'required' => false,
            'label' => 'socialFacebook',
            'value' => $this->_social['facebook'],
            'readonly' => false,
            'class' => 'span8',
            'placeholder' => '',
            'validators' => array($urlValidator),
            'decorators' => array($textDecorator)
        )));

        $this->addElement($this->createElement('text', 'twitter', array(
            'required' => false,
            'label' => 'socialTwitter',
            'value' => $this->_social['twitter'],
            'readonly' => false,
            'class' => 'span8',
            'placeholder' => '',
            'validators' => array($urlValidator),
            'decorators' => array($textDecorator)
        )));

        $this->addElement($this->createElement('text', 'odnoklassniki', array(
            'required' => false,
            'label' => 'socialOdnoklassniki',
            'value' => $this->_social['odnoklassniki'],
            'readonly' => false,
            'class' => 'span8',
            'placeholder' => '',
            'validators' => array($urlValidator),
            'decorators' => array($textDecorator)
        )));

        $this->addElement($this->createElement('text', 'instagram', array(
            'required' => false,
            'label' => 'socialInstagram',
            'value' => $this->_social['instagram'],
            'readonly' => false,
            'class' => 'span8',
            'placeholder' => '',
            'validators' => array($urlValidator),
            'decorators' => array($textDecorator)
        )));

        $this->addElement($this->createElement('text', 'address', array(
            'required' => true,
            'label' => 'address',
            'value' => $this->_contacts['address'],
            'readonly' => false,
            'class' => 'span8',
            'placeholder' => '',
            'decorators' => array($textDecorator)
        )));

        $this->addElement($this->createElement('text', 'phone', array(
            'required' => true,
            'label' => 'phone',
            'value' => $this->_contacts['phone'],
            'readonly' => false,
            'class' => 'span8',
            'placeholder' => '',
            'validators' => array($phoneValidator),
            'decorators' => array($textDecorator)
        )));

        $this->addElement($this->createElement('text', 'email', array(
            'required' => true,
            'label' => 'email',
            'value' => $this->_contacts['email'],
            'readonly' => false,
            'class' => 'span8',
            'placeholder' => '',
            'validators' => array($mailValidator),
            'decorators' => array($textDecorator)
        )));

        $this->addElement($this->createElement('text', 'discountOne', array(
            'required' => true,
            'label' => 'discountOne',
            'value' => $this->_discountSettings['discountOne'],
            'readonly' => false,
            'class' => 'span8',
            'placeholder' => '',
            'validators' => array(['Digits']),
            'decorators' => array($textDecorator)
        )));
        $this->addElement($this->createElement('text', 'discountTwo', array(
            'required' => true,
            'label' => 'discountTwo',
            'value' => $this->_discountSettings['discountTwo'],
            'readonly' => false,
            'class' => 'span8',
            'placeholder' => '',
            'validators' => array(['Digits']),
            'decorators' => array($textDecorator)
        )));

        $this->addElement($this->createElement('submit', 'submit', array (
            'label' => 'mainSettingsSave',
            'decorators' => array($buttonDecorator)
        )));
    }
}
