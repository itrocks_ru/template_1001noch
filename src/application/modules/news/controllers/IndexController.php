<?php
class News_IndexController extends ItRocks_Controller_Action {

    public function init() {
        $tagsModel = new Model_Tags();
        $tags = $tagsModel->getTagsByMCA(
            $this->_request->getModuleName(),
            $this->_request->getControllerName(),
            'index'
        );

        $this->_addMeta($tags['metaTitle'], $tags['metaDescription']);
        $this->view->assign('title', $tags['title']);
    }

    public function indexAction() {
        $page = $this->_request->getParam('page', 1);
        $paginator = $this->_getNewsPaginator($page);
        $pagesAmount = $paginator->getPages()->pageCount;
        $this->view->paginator = $paginator;
        $this->view->assign('pagesAmount', $pagesAmount);
    }
    
    public function showAction() {
        $itemAlias  = $this->_request->getParam('alias');
        $newsTable = new News_Model_DbTable_Table();
        $news = $newsTable->getByAlias($itemAlias);
        
        if ($news){
//            $tagsTable  = new News_Model_Tags;
//            $tags       = $tagsTable->getAllTagsFromArticle($news->id);
//            $newsTags   = $tagsTable->getAllTags();

            $this->view->headTitle()->prepend($news->metaTitle);
            $this->view->headMeta()->setName('description',$news->metaDescription);
//            $this->view->assign('newsTags', $newsTags);
//            $this->view->assign('tags', $tags);
            $this->view->assign('news', $news);
        } else {
            $this->getResponse()->setHttpResponseCode(404);
            $this->view->setBasePath(APPLICATION_PATH . '/views/');
            $this->_helper->viewRenderer->renderBySpec('error', ['module' => 'default', 'controller' => 'error']);
            $this->_addMeta('404! Страница не существует');
        }
    }

    protected function _newsNotFound() {
        $this->_helper->flashMessenger->setNamespace('errorMessages')
                                      ->addMessage($this->view->translate('notFound'));
        
        $this->_helper->redirector('index');
    }

    public function showByTagAction()
    {
        $this->_helper->viewRenderer('index');
        $tagId = $this->_request->getParam('tag');
        $page = $this->_request->getParam('page', 1);

        $news       = new News_Model_News();
        $newsByTag  = $news->getSortedNewsByTag($tagId);

        $tag        = new Model_DbTable_Tags();
        $tagName    = $tag->getTagById($tagId);

        $this->view->headTitle()->prepend(sprintf($this->view->translate('newsShowByTag'), $tagName['name']));

        $paginator = Zend_Paginator::factory($newsByTag);
        $paginator->setItemCountPerPage(3);
        $paginator->setCurrentPageNumber($page);
        $pagesAmount = $paginator->getPages()->pageCount;
        $this->view->assign('pagesAmount', $pagesAmount);

        $this->view->assign('paginator', $paginator);
        $this->view->assign('helper', $this->_helper->textHelper);
    }

    /**
     * TODO Сделать рефакторинг тегов
     */
    public function ajaxGetNewsAction(){
        $this->_helper->layout()->disableLayout();
        $page = $this->_request->getParam('page', 1);

        $this->view->paginator = $this->_getNewsPaginator($page);
    }

    public function newsWidgetAction() {
        $newsTale = new News_Model_DbTable_Table();
        $items = $newsTale->getLastNews(3);

        $this->view->assign('items', $items);
    }

    private function _getNewsPaginator($page) {
        $news = new News_Model_DbTable_Table();
        $allNews = $news->getAllNews();

        $paginator = Zend_Paginator::factory($allNews);
        $paginator->setItemCountPerPage(3);
        $paginator->setCurrentPageNumber($page);
        return $paginator;
    }
}
