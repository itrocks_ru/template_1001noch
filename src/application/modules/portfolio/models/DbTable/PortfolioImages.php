<?php

class Portfolio_Model_DbTable_PortfolioImages extends Zend_Db_Table_Abstract {
    
  protected $_name = 'PortfolioImages';
  protected $_primary = 'id';
   
  public function add($filename, $portfolioId) {
    $data = array(
      'image' => $filename,
      'portfolioId' => $portfolioId
    );

    $this->insert($data);
  }

  public function getImagesByPortfolioId($portfolioId) {
    $select = $this->select()
		   ->from($this->_name, array('image', 'id'))
	           ->where('portfolioId = ?', $portfolioId);
    return $this->getAdapter()->fetchAll($select);
  }

}
