<?php

class Blog_IndexController extends ItRocks_Controller_Action
{
    public function init()
    {
        $tagsModel = new Model_Tags();
        $tags = $tagsModel->getTagsByMCA(
            $this->_request->getModuleName(),
            $this->_request->getControllerName(),
            $this->_request->getActionName()
        );
        $this->_addMeta($tags['metaTitle'], $tags['metaDescription']);
        $this->view->assign('title', $tags['title']);
    }

    public function indexAction()
    {
        $page = $this->_request->getParam('page', 1);
        $diyTable = new Blog_Model_DbTable_Blog;
        $tagsTable = new Blog_Model_Tags();
        $blogTags = $tagsTable->getAllTags();
        $items = $diyTable->getAll();
        $diyCommentsTable = new Blog_Model_DbTable_BlogComments();
        foreach ($items as $item => $value) {
            $comments = $diyCommentsTable->getCommentsByDiyId($items[$item]['id']);
            $commentsCount = count($comments);
            foreach ($comments as $key => $val) {
                if ($val['reply']){
                    $commentsCount++;
                }
            }
            $items[$item]['comment_count'] = $commentsCount;
        }
        $paginator = Zend_Paginator::factory($items);
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($page);
        $currentPageNumber = $paginator->getCurrentPageNumber();
        $requestPageNumber = (int)$this->_request->getParam('page');

        if (($currentPageNumber < $requestPageNumber) || (!is_int($requestPageNumber))) {
            $this->error404();
        }
        $this->view->assign('blogTags', $blogTags);
        $this->view->assign('paginator', $paginator);
        $this->view->assign('helper', $this->_helper->textHelper);
    }

    public function showAction()
    {
        $itemAlias  = $this->_request->getParam('alias');
        $diy = null;
        if (!empty($itemAlias)){
            $diyTable   = new Blog_Model_DbTable_Blog;
            $diy        = $diyTable->getByAlias($itemAlias);
        }


        if ($diy){
            $tagsTable  = new Blog_Model_Tags;
            $blogTags   = $tagsTable->getAllTags();
            $tags       = $tagsTable->getAllTagsFromArticle($diy->id);

            $this->view->headTitle()->prepend($diy->metaTitle);
            $this->view->headMeta()->setName('description',$diy->metaDescription);

            $diyCommentsTable = new Blog_Model_DbTable_BlogComments();
            $comments = $diyCommentsTable->getCommentsByDiyId($diy->id);
            $commentsCount = count($comments);
            foreach ($comments as $key => $value) {
                if ($value['reply']){
                    $commentsCount++;
                }
            }

            $form = new Blog_Form_Comment();

            if ($this->_request->isPost()) {
                $formData = $this->_request->getPost();
                if ($form->isValid($formData)) {
                    $data = array(
                        'date' => date('Y-m-d'),
                        'username' => $formData['username'],
                        'email' => $formData['email'],
                        'new' => 1,
                        'comment' => $formData['comment'],
                        'status' => 1,
                        'diyId' => $diy->id
                    );
                    $comment = $diyCommentsTable->fetchNew();
                    $comment->setFromArray($data);
                    $comment->save();

                    $this->_helper->flashMessenger->setNamespace('messages')
                        ->addMessage($this->view->translate('blogCommentAddSuccess'));

                    $this->_helper->redirector('show', 'index', 'blog', array('alias' => $diy->alias));
                }
            }
            $this->view->assign('blogTags', $blogTags);
            $this->view->assign('tags', $tags);
            $this->view->assign('item', $diy);
            $this->view->assign('commentsCount', $commentsCount);
            $this->view->assign('comments', $comments);
            $this->view->assign('form', $form);
        } else{
            $this->error404();
        }

    }

    public function showByTagAction()
    {
        $this->_helper->viewRenderer('index');
        $page = $this->_request->getParam('page', 1);
        $tag = $this->_request->getParam('tag');
        $tagsTable = new Model_DbTable_Tags;
        $diyTable = new Blog_Model_DbTable_Blog;
        $items = $diyTable->getAllByTag($tag);
        $tagName = $tagsTable->getTagById($tag);

        $this->view->headTitle()->prepend(sprintf($this->view->translate('blogShowByTag'), $tagName['name']));

        $paginator = Zend_Paginator::factory($items);
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($page);
        $blogTags = $tagsTable->getAllBlogTagsSortByCount();

        $this->view->assign('blogTags', $blogTags);
        $this->view->assign("showTagMode", true);
        $this->view->assign('tagName', $tagName['name']);
        $this->view->assign('paginator', $paginator);
        $this->view->assign('helper', $this->_helper->textHelper);
    }
}