<?php

class Blog_Model_Tags
{
    /**
     * @param $tags
     * @param $articleId
     */
    public function updateTagsByArticle($articleId, $tags){

        $blogTagsTable = new Blog_Model_DbTable_BlogTags();
        $tagsTable = new Model_DbTable_Tags();

        $wereTags   = $this->getAllTagsFromArticle($articleId, true);
        $tags = $this->clearTagsValue($tags);

        if (!empty($wereTags)){
            $wereTags   = $this->clearTagsValue($wereTags);
            $result     = array_diff($tags, $wereTags);             // get new tags
            $remoteTags = array_diff($wereTags, $tags);             // get remote tags
            $this->removeLinksBlogTags($articleId, $remoteTags);    // remove
        } else{
            $result = $tags;
        }

        foreach($result as $tag)
        {
            $tagFromDb = $tagsTable->searchTag($tag);

            if(empty($tagFromDb))
            {
                //Сначала добавим тег
                $tagId = $tagsTable->addTag($tag);
            }
            else
            {
                $tagId = $tagFromDb['id'];
            }
            $blogTagsTable->insert(['idBlog' => $articleId, 'idTags' => $tagId]);
        }
    }
    /**
     * @param $articleId
     * @return string
     */
    public function getAllTagsFromArticle($articleId, $getString = false)
    {
        $tagsTable = new Model_DbTable_Tags();
        $tags = $tagsTable->getTagsByBlogArticle($articleId);
        $return = "";

        if ($getString === true)
        {
            foreach($tags as $tag)
            {
                $return .= $tag['name'].", ";
            }
        } else{
            $return = $tags;
        }
        return $return;
    }


    public function getAllTags()
    {
        $tagsTable = new Model_DbTable_Tags();
        $tags = $tagsTable->getAllBlogTagsSortByCount();
        return $tags;
    }

    private function cmp($a, $b)
    {
        return strnatcmp($a["name"], $b["name"]);
    }

    private function clearTagsValue($tags){
        if (!is_array($tags)){
            $tags = explode(",", $tags);
        }
        foreach ($tags as $tag => $value){
            $tags[$tag] = trim($value);
        }
        $tags = array_diff($tags, array('', ' '));
        $tags = array_unique($tags);
        return $tags;
    }
    private function removeLinksBlogTags($articleId, $remoteTags){
        $blogTagsTable = new Blog_Model_DbTable_BlogTags();
        $tagsTable = new Model_DbTable_Tags();
        if (!empty($remoteTags)){
            $searchResault = $tagsTable->searchIdbyTagsName($remoteTags);
            $blogTagsTable->removeLinks($articleId,$searchResault);
        }
    }
}