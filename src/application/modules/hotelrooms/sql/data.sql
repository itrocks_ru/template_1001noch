INSERT INTO `HotelRooms_Categories` (`id`, `title`, `alias`, `position`) VALUES
  (1,	'Основная категория',	'osnovnaya-kategoriya',	'1');

INSERT INTO `HotelRooms_Rooms` (`id`, `categoryId`, `title`, `alias`, `pricePerDay`, `pricePerHour`, `shortDescription`, `description`, `personAmount`, `photo`, `bed`, `fridge`, `jacuzzi`, `satTv`, `electrofireplace`, `sauna`, `wifi`, `shower`, `fireplace`, `conditioner`, `cupboard`, `minibar`) VALUES
  (1,	1,	'Красный зал',	'krasnyy-zal',	'2000р',	'',	'<p>Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда приемлемые модификации, например, юмористические вставки или слова, которые даже отдалённо не напоминают латынь.</p>\r\n',	'<p>Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда приемлемые модификации, например, юмористические вставки или слова, которые даже отдалённо не напоминают латынь.</p>\r\n',	'12',	'big-krasnyy-zal-foto-1.jpg',	1,	1,	0,	0,	1,	1,	1,	1,	1,	0,	0,	0),
  (2,	1,	'Синий зал',	'siniy-zal',	'2000',	'2000',	'<p>Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда приемлемые модификации, например, юмористические вставки или слова, которые даже отдалённо не напоминают латынь.</p>\r\n',	'<p>Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда приемлемые модификации, например, юмористические вставки или слова, которые даже отдалённо не напоминают латынь.</p>\r\n',	'12',	'big-siniy-zal-foto-1.jpg',	1,	1,	0,	1,	0,	0,	1,	1,	1,	0,	0,	0),
  (3,	1,	'Зеленый зал',	'zelenyy-zal',	'2000',	'2000',	'<p>Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда приемлемые модификации, например, юмористические вставки или слова, которые даже отдалённо не напоминают латынь.</p>\r\n',	'<p>Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда приемлемые модификации, например, юмористические вставки или слова, которые даже отдалённо не напоминают латынь.</p>\r\n',	'12',	'big-zelenyy-zal-foto-1.jpg',	1,	1,	0,	1,	0,	0,	1,	1,	1,	0,	0,	0);


INSERT INTO `HotelRooms_Photos` (`id`, `roomId`, `photo`, `position`) VALUES
  (1,	1,	'big-krasnyy-zal-foto-2.jpg',	0),
  (2,	1,	'big-krasnyy-zal-foto-3.jpg',	0),
  (3,	1,	'big-krasnyy-zal-foto-4.jpg',	0),
  (5,	2,	'big-siniy-zal-foto-6.jpg',	0),
  (6,	3,	'big-zelenyy-zal-foto-2.jpg',	0);