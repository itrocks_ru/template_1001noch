INSERT INTO `Photos` (`id`, `src`, `cutSrc`, `title`, `desc`, `date`) VALUES
  (1,	'images/photos/2018022803034504829600.jpg',	'images/photos/cut/2018022803034504834200.jpg',	'1',	'',	'2018-02-28 15:03:45'),
  (2,	'images/photos/2018022803035359634800.jpg',	'images/photos/cut/2018022803035359641400.jpg',	'2',	'',	'2018-02-28 15:03:53'),
  (3,	'images/photos/2018022803040132472800.jpg',	'images/photos/cut/2018022803040132479800.jpg',	'3',	'',	'2018-02-28 15:04:01'),
  (4,	'images/photos/2018022803040723345200.jpg',	'images/photos/cut/2018022803040723349100.jpg',	'4',	'',	'2018-02-28 15:04:07'),
  (5,	'images/photos/2018022803072092317100.jpg',	'images/photos/cut/2018022803072092321200.jpg',	'1',	'',	'2018-02-28 15:07:20'),
  (6,	'images/photos/2018022803072743350300.jpg',	'images/photos/cut/2018022803072743354400.jpg',	'2',	'',	'2018-02-28 15:07:27'),
  (7,	'images/photos/2018022803073517340400.jpg',	'images/photos/cut/2018022803073517346600.jpg',	'3',	'',	'2018-02-28 15:07:35'),
  (8,	'images/photos/2018022803074154284600.jpg',	'images/photos/cut/2018022803074154289600.jpg',	'4',	'',	'2018-02-28 15:07:41');

INSERT INTO `Photo_Categories` (`id`, `pid`, `title`, `desc`, `alias`, `metaTitle`, `metaDesc`, `date`, `image`) VALUES
  (1,	0,	'Основная категория',	'',	'osnovnaya-kategoriya',	'',	'',	'2018-02-28 15:03:36',	'phpr9vXN2');

INSERT INTO `Photo_Relations` (`id`, `photo_id`, `category_id`) VALUES
  (1,	5,	1),
  (2,	6,	1),
  (3,	7,	1),
  (4,	8,	1);