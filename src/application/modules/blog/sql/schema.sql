DROP TABLE IF EXISTS `Blog`;
CREATE TABLE `Blog` (
  `id` int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `title` varchar(250) NOT NULL,
  `shortDescription` text NOT NULL,
  `description` text NOT NULL,
  `image` varchar(250) NOT NULL,
  `date` date NOT NULL,
  `alias` varchar(250) NOT NULL,
  `metaTitle` varchar(250) NOT NULL,
  `metaDescription` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `BlogComments`;
CREATE TABLE `BlogComments` (
  `id` int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `username` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `comment` text NOT NULL,
  `reply` text NOT NULL,
  `new` tinyint NOT NULL,
  `date` date NOT NULL,
  `status` tinyint(1) NOT NULL,
  `diyId` int unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `BlogTags`;
CREATE TABLE `BlogTags` (
  `idBlog` int(10) unsigned NOT NULL,
  `idTags` int(11) unsigned NOT NULL,
  KEY `idTags` (`idTags`),
  KEY `idBlog` (`idBlog`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `Tags`;
CREATE TABLE `Tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;