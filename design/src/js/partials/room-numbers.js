$(document).ready(function() {
    var categoriesAmount = $('.rooms-list-room-types-list li').length;
    var liWidth = Math.round(100 /categoriesAmount);
    $('.rooms-list-room-types-list > li').each(function() {
        $(this).css('width', liWidth + '%');
    });
    var initialIndicatorOffset = Math.round((liWidth / 3) + 5);
    $('.room-list-category-indicator').css('left', initialIndicatorOffset + '%');





    $('.room-page-booking-btn').click(function() {
        var id = $(this).data('id');
        $('#booking-room').val(id);
        $('#bookingModal').modal('toggle');
        return false;
    });
    $(document).on('click', '.btn-room-booking', function() {
        var id = $(this).data('id');
        $('#booking-room').val(id);
        $('#bookingModal').modal('toggle');
        return false;
    });
    $('#booking-form').submit(function() {
        var room = $('#booking-room').val();
        var phone = $('#booking-phone').val();
        var fio = $('#booking-fio').val();

        $.ajax({
            url: config.hotelroom.request,
            method: 'POST',
            data: {
                room: room,
                phone: phone,
                fio: fio
            },
            dataType: 'json',
            success: function(response) {
                if (typeof response.success !== 'undefined') {
                    location.href = config.hotelroom.successUrl;
                } else if (typeof response.error !== 'undefined') {
                    if (typeof response.error.fio !== 'undefined') {
                        $('#booking-fio-error').html(response.error.fio);
                    }
                    if (typeof response.error.phone !== 'undefined') {
                        $('#booking-phone-error').html(response.error.phone);
                    }
                }
            },
            error: function(errors) {
                $('#booking-common-error').html('Произошла непридвиненная ошибка. Приносим извинения');
                return false;
            }
        });
        return false;
    });

    $('.room-category').click(function () {
        var number = $(this).data('id');
        var alias = $(this).data('alias');
        $.ajax({
            url: config.hotelroom.changeCategoryUrl,
            method: 'GET',
            data: {
                alias: alias
            },
            dataType: 'HTML',
            success: function(response) {
                var indicatorOffset = Math.round(((number - 1) * liWidth) + (liWidth / 3) + 5);
                $('.rooms-list-container').html(response);
                $('.room-list-category-indicator').css('left', indicatorOffset + '%');
                return false;
            },
            error: function(errors) {
                alert('Произошла ошибка на стороне сервере. Приносим извинения');
                return false;
            }
        });
        return false;
    });

});