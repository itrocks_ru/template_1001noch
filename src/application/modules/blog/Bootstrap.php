<?php

class Blog_Bootstrap extends Zend_Application_Module_Bootstrap{

  protected function _initRouter(){
    $frontController = Zend_Controller_Front::getInstance();
    $router = $frontController->getRouter();

      $router->addRoute('/blog/show', new Zend_Controller_Router_Route(
          '/blog/:alias', array(
              'module' => 'blog',
              'controller' => 'index',
              'action' => 'show',
              'alias' => null
          )
      ));
      $router->addRoute('blogPages', new Zend_Controller_Router_Route(
          '/blog/:page', array(
              'module' => 'blog',
              'controller' => 'index',
              'action' => 'index',
              'page' => null
          ),
          array('page' => '\d+')
      ));
      $router->addRoute('/blog/showtag', new Zend_Controller_Router_Route(
          '/blog/showtag/:tag', array(
          'module' => 'blog',
          'controller' => 'index',
          'action' => 'show-by-tag'
      )));

      $router->addRoute('blogAdmin', new Zend_Controller_Router_Route(
          '/blog/admin', array(
              'module' => 'blog',
              'controller' => 'admin',
              'action' => 'index'
          )
      ));
    
    return $router;
  }
    
}
